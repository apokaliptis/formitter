package formitter

import (
	"net/mail"
	"net/smtp"
	"strings"
	"unicode"
)

type Form interface {
	ToEmail() *Email
}

type Email struct {
	Format  string
	From    *mail.Address
	ReplyTo *mail.Address
	To      []*mail.Address
	Subject string
	Body    []byte
}

type SendConfig struct {
	Host   string
	Port   string
	User   string
	Passwd string
}

// Generate message
const emailTmpl string = `MIME-version: 1.0;
Content-Type: {{ .Format }}; charset=\"UTF-8\";
X-Mailer: SubmitForm-v0.0.0;
From: {{ with .From.Name }}{{ . }} {{ end }}<{{ .From.Address }}>
{{- with .ReplyTo }}
Reply-To: {{ . }}{{ end }}
To: {{ range (slice .To 0 1) }}{{ with .Name }}{{ . }} {{ end }}<{{ .Address }}>{{ end }}
{{- range (slice .To 1 (len .To)) }},{{ with .Name }}{{ . }} {{ end }}<{{ .Address }}>{{ end }}
Subject: {{ .Subject }}

{{ printf "%s" .Body }}`

// Validate and email form to FORM_MAILTO env email.
func (email *Email) Send(c *SendConfig) error {
	// Filter non-ASCII and newline chars.
	email.Format = cleanHeader(email.Format)
	email.Subject = cleanSubject(email.Subject)

	// Generate email message.
	msg := ParseTxtTmpl(email, emailTmpl)

	// Get plain email list.
	size := len(email.To)
	to := make([]string, size)
	for i, e := range email.To {
		to[i] = e.Address
	}

	// Configure authentication.
	auth := smtp.PlainAuth("", c.User, c.Passwd, c.Host)

	// Send mail
	if err := smtp.SendMail(c.Host+":"+c.Port, auth, c.User, to, msg); err != nil {
		return err
	}

	return nil
}

// Returns filtered string for email header.
func cleanHeader(str string) string {
	return strings.Map(func(r rune) rune {
		switch {
		case r == '\n':
			return ' '
		case r == '\r', r == ':', r > unicode.MaxASCII:
			return -1
		default:
			return r
		}
	}, str)
}

// Returns filtered string for email subject.
func cleanSubject(str string) string {
	return strings.Map(func(r rune) rune {
		switch {
		case r == '\n':
			return ' '
		case r == '\r', r > unicode.MaxASCII:
			return -1
		default:
			return r
		}
	}, str)
}
