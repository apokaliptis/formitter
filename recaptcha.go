package formitter

import (
	"encoding/json"
	"errors"
	"net/http"
	"strings"
	"time"
)

const siteVerifyURL = "https://www.google.com/recaptcha/api/siteverify"

type SiteVerifyResponse struct {
	Success     bool      `json:"success"`
	Score       float64   `json:"score"`
	Action      string    `json:"action"`
	ChallengeTS time.Time `json:"challenge_ts"`
	Hostname    string    `json:"hostname"`
	ErrorCodes  []string  `json:"error-codes"`
}

func VerifyRecaptcha(action, ip, response, secret string) error {
	req, err := http.NewRequest(http.MethodPost, siteVerifyURL, nil)
	if err != nil {
		return err
	}

	// Add necessary request parameters.
	q := req.URL.Query()
	q.Add("secret", secret)
	q.Add("response", response)
	q.Add("remoteip", ip)
	req.URL.RawQuery = q.Encode()

	// Make request
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// Decode response.
	var body SiteVerifyResponse
	if err = json.NewDecoder(resp.Body).Decode(&body); err != nil {
		return err
	}

	// Check recaptcha verification success.
	if !body.Success {
		errMsg := "unsuccessful reCAPTCHA request"
		if len(body.ErrorCodes) > 0 {
			errMsg += ": " + strings.Join(body.ErrorCodes, ", ")
		}
		return errors.New(errMsg)
	}

	// Check response score.
	if body.Score < 0.5 {
		return errors.New("trust level to low")
	}

	// Check response action.
	if body.Action != action {
		return errors.New("mismatched recaptcha action")
	}

	return nil
}
