package formitter

import (
	"fmt"
	"net/http"

	"github.com/go-playground/form/v4"
	"github.com/go-playground/validator/v10"
)

// Validate and decode form submission
func DecodeForm(r *http.Request, submission any) error {

	// Decode form data from http.Request into ContactForm struct.
	decoder := form.NewDecoder()

	if err := decoder.Decode(submission, r.Form); err != nil {
		return fmt.Errorf("failed to decode form: %v", err)
	}

	// VALIDATE INPUTS

	validate := validator.New()
	if err := validate.Struct(submission); err != nil {
		return fmt.Errorf("invalid form submission: %v", err)
	}

	return nil
}
