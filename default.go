package formitter

import (
	"log"
	"net/http"
	"net/mail"
	"os"
)

/* Helpful functions for quickly making forms with Formitter. */

func DefaultSubmit(w http.ResponseWriter, r *http.Request, submission Form, action string, honeypots ...string) {

	// Check request method.
	if r.Method != "POST" {
		// Reject all non-POST requests.
		errMsg := "Invalid request type: " + r.Method
		w.Header().Set("Allow", "POST")
		http.Error(w, errMsg, http.StatusMethodNotAllowed)
		log.Println(errMsg)
		return
	}

	if caught, pots := CheckPots(r, honeypots...); caught {
		http.Error(w, "Unauthorized", http.StatusUnauthorized)
		log.Printf("Caught bot via: %v\n", pots)
		return
	}

	// Verify reCAPTCHA.
	if err := VerifyRecaptcha(action, r.RemoteAddr,
		r.PostFormValue("g-recaptcha-response"),
		os.Getenv("RECAPTCHA_V3_SECRET_KEY"),
	); err != nil {
		http.Error(w, "Unauthorized", http.StatusUnauthorized)
		log.Printf("reCAPTCHA failed: %v\n", err)
		return
	}

	// Decode form submission.
	if err := DecodeForm(r, submission); err != nil {
		errMsg := "Invalid form: " + err.Error()
		http.Error(w, errMsg, http.StatusUnprocessableEntity)
		log.Println(errMsg)
		return
	}

	// Generate email.
	email := submission.ToEmail()

	// Send message.
	if err := email.Send(DefaultSendConfig()); err != nil {
		http.Redirect(w, r, "/senderror", http.StatusSeeOther)
		log.Printf("Failed to send message: %v", err)
		return
	}

	http.Redirect(w, r, "/thanks", http.StatusSeeOther)
}

func DefaultToEmail(submission Form, template, subject string) *Email {
	var err error
	var email Email

	// Set email format
	email.Format = "text/html"

	// Get from address.
	from := os.Getenv("FORM_FROM")
	if from == "" {
		// Fallback to SMTP_USERNAME
		from = os.Getenv("SMTP_USERNAME")
	}
	email.From, err = mail.ParseAddress(from)
	if err != nil {
		log.Panicf("Failed to parse sender address: %v", err)
	}

	// Get To addresses.
	email.To, err = mail.ParseAddressList(os.Getenv("FORM_TO"))
	if err != nil {
		log.Panicf("Failed to parse address list: %v", err)
	}

	// Set subject line.
	email.Subject = subject

	// Generate email body.
	email.Body = ParseHtmlTmpl(submission, template)

	return &email
}

func DefaultSendConfig() *SendConfig {
	var config SendConfig
	config.Host = os.Getenv("SMTP_HOST")
	config.Port = os.Getenv("SMTP_PORT")
	config.User = os.Getenv("SMTP_USERNAME")
	config.Passwd = os.Getenv("SMTP_PASSWD")

	if config.User == "" {
		// Fallback to FORM_FROM.
		user, err := mail.ParseAddress(os.Getenv("FORM_FROM"))
		if err != nil {
			log.Panicf("Failed to find SMTP_USERNAME")
		}
		config.User = user.Address
	}

	return &config
}

func NameAndEmail(name, email string) string {
	return name + " <" + email + ">"
}
