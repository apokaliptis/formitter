package formitter

import (
	"bytes"
	"log"
	"text/template"
)

// Parse template string and...
func ParseTxtTmpl(data any, strTmpl string) []byte {
	// Generate form email.
	tmpl, err := template.New("").Parse(strTmpl)
	if err != nil {
		log.Panicf("Failed to parse template: %v", err)
	}

	buf := new(bytes.Buffer)
	tmpl.Execute(buf, data)

	return buf.Bytes()
}
