package formitter

import "net/http"

func CheckPots(r *http.Request, honeypots ...string) (bool, []string) {
	// List of honeypots that caught a bot.
	caught := make([]string, 0, len(honeypots))

	// Check honeypots
	for _, pot := range honeypots {
		catch := r.PostFormValue(pot)
		if catch != "" {
			caught = append(caught, pot)
		}
	}

	if len(caught) > 0 {
		return true, caught
	}
	return false, nil
}
