package formitter

import (
	"bytes"
	"html/template"
	"log"

	"github.com/Masterminds/sprig/v3"
)

// Parse template string and...
func ParseHtmlTmpl(data any, strTmpl string) []byte {
	// Generate form email.
	tmpl, err := template.New("").Funcs(sprig.FuncMap()).Parse(strTmpl)
	if err != nil {
		log.Panicf("Failed to parse template: %v", err)
	}

	buf := new(bytes.Buffer)
	tmpl.Execute(buf, data)

	return buf.Bytes()
}
